##
# @author Anna Nguyen s154230
##

 	Feature: Token features
 		
 		Scenario: Getting registered
 		Given that I am "not registered"   
 		When I request 1 tokens 
 		Then I have 1 token
 		And I am registered
 		
 		Scenario: Request token when registered with five used tokens
 		Given that I am "registered"
 		And that I have 5 tokens
 		And that I have 5 used tokens
 		When I request 1 tokens
 		Then I have 1 unused token
 		
 		Scenario: Request token when registered with two unused tokens
 		Given that I am "registered"
 		And that I have 2 tokens
 		When I request 1 tokens
 		Then there is an error message "Too many unused tokens"
 		
 		Scenario: Request six tokens
 		Given that I am "registered"
 		When I request 6 tokens
 		Then there is an error message "Can only request 1-5 tokens"
 		
 		Scenario: Request no tokens
 		Given that I am "registered"
 		When I request 0 tokens
 		Then there is an error message "Can only request 1-5 tokens"
 		
 		Scenario: Use an already used token
 		Given that I am "registered"
 		And that I have 1 tokens
 		And that I have 1 used tokens
 		When I use a token
 		Then there is an error message "Token has already been used"
 		
 		Scenario: Use an unused token
 		Given that I am "registered"
 		And that I have 1 tokens
 		When I use a token
 		Then I have 0 unused token
 		
 		Scenario: Use a fake token
 		Given that I am "registered"
 		When I use a fake token
 		Then there is an error message "Token does not exist"