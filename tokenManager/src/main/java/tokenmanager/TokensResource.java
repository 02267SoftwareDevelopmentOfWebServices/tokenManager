package tokenmanager;


import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * Resource for set of tokens
 * 
 * @author Anna Nguyen s154230
 *
 */

@Path("/tokens")
public class TokensResource {

	public static RPCInterface rpcInterface = new RPCClient();
	static TokenManager tm = new TokenManager(rpcInterface);

	/**
	 * Called via REST to assert a customer's number of unused tokens.
	 * 
	 * @param customerId is the id of the customer
	 * @return a response containing the number of unused tokens 
	 */
	@GET
	@Produces("text/plain")
	public Response numberOfUnusedTokens(@QueryParam("customerid") String customerId) {
		try {
			String number = ""+tm.numberOfUnusedTokens(customerId);
			return Response.ok(number).build();

		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
	
	/**
	 * Called via REST to clear the database after each system test
	 */
	@PUT
	@Produces("text/plain")
	public Response clearDatabase() {
		try {
			tm.clearDatabase();
			return Response.ok("true").build();
		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
	

	/**
	 * Called via REST to request a specific number of tokens.
	 * 
	 * @param customerId is the id of the customer who wishes to request tokens
	 * @param amount is the number of tokens the customer wants to request
	 * @return a response containing a list of tokens
	 */
	
	// POST, because requestToken is not idempotent
	@POST
	@Consumes("text/plain")
	@Produces(MediaType.APPLICATION_JSON)
	//public Response requestTokens(String customerId, @QueryParam("amount") int amount) {
	public List<Token> requestTokens(String customerId, @QueryParam("amount") int amount) {
		try {
			
			//List<Token> tokens = tm.requestTokens(customerId ,amount);
			/*
			JSONObject tokenObj = new JSONObject();
			tokenObj.put("tokens", tokens);

			return Response.ok(tokenObj.toString()).build();
			*/
			
			return tm.requestTokens(customerId ,amount);
			//GenericEntity<List<Token>> entities = new GenericEntity<List<Token>>(tokens){};
			//return Response.ok(tm.requestTokens(customerId ,amount)).build();
			//return Response.ok(tokens).build();
			

		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
}

