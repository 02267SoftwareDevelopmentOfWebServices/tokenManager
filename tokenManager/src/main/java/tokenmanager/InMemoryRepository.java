package tokenmanager;

import java.util.ArrayList;
import java.util.List;

/**
 * In-memory repository simulating a database
 * 
 * @author Anna Nguyen s154230
 *
 */

public class InMemoryRepository implements Repository {
	List<Customer> customers = new ArrayList<Customer>();
	
	
	/**
	 * This method retrieves a customer object.
	 * 
	 * @param id is the id of the customer to be retrieved
	 */
	// Check if customer ID already exists in DB.
	// Used when trying to register a new customer.
	public Customer getCustomer(String id){
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getID().equals(id)){
				return customers.get(i);
			}
		}
		//TODO Throw error
		return null;
	}

	/**
	 * This method clears the repository
	 * by clearing the list of tokens and customers.
	 */
	
	public void clearRepository(){
		customers.clear();
	}
	
	/**
	 * This method adds a customer to the repository.
	 * 
	 * @param customer is the customer to be added
	 */
	public void addCustomer(Customer customer) {
		customers.add(customer);
	}

	/**
	 * This method removes a customer from the repository.
	 * 
	 * @param customer is the customer to be removed
	 */
	public void removeCustomer(Customer customer) {
		customers.remove(customer);
	}

	/**
	 * This method updates the list of tokens belonging to a customer.
	 * 
	 * @param customer is the customer whose tokens must be updated
	 * @param newTokens is the list of updated tokens
	 */
	public void updateCustomer(Customer customer, List<Token> newTokens) {
		customers.remove(customer);
		customer.updateTokens(newTokens);
		customers.add(customer);
	}

	
	/**
	 * This method retrieves the list of tokens belonging to a customer.
	 * 
	 * @param customerId is the id of a customer
	 * @return the customer's list of tokens
	 */
	public List<Token> getCustomerTokens(String customerId) {
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getID().equals(customerId)){
				return customers.get(i).getTokens();
			}
		}
		return null;
	}
	
	
	/**
	 * This method retrieves all customers in the repository.
	 * 
	 * @return a list of customers
	 */
	public List<Customer> getAllCustomers() {
		return customers;
	}
}
