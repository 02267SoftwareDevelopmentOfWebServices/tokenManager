package tokenmanager;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Interface for RPC client 
 * Created for dependency inversion
 * 
 * @author Anna Nguyen s154230
 *
 */

public interface RPCInterface {
	
	public String call(String message) throws IOException, InterruptedException;

	public void close() throws IOException;

}
