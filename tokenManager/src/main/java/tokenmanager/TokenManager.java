package tokenmanager;

import java.util.ArrayList;

import java.util.List;
import java.util.UUID;
import javax.inject.Inject;


import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeFactory;

/**
 * Token manager microservice
 * 
 * @author Anna Nguyen s154230
 *
 */


public class TokenManager {

	// In-memory database
	private static Repository repo = new InMemoryRepository();

	RPCInterface rpcInterface;
	//RPCClient tokenAccountRpc;

	public static final int BAR_HEIGHT = 60;
	public static final int BAR_WIDTH = 2;

	public TokenManager(RPCInterface rpcInterface) {
		this.rpcInterface = rpcInterface;
	}


	/**
	 * This method allows a customer to request tokens.
	 * 
	 * @param customerId is the CPR number of the customer
	 * @param amount is the number of tokens the customer is requesting
	 * @return a list of Token objects
	 * @throws Exception on too many unused tokens or on no bank account.
	 */
	public  List<Token> requestTokens(String customerId, int amount) throws Exception {
		if (amount < 1 || amount > 5 ){
			throw new Exception("Can only request 1-5 tokens");
		}
		// If user is registered
		// check if they have at most one unused token left
		List<Token> newTokens = new ArrayList<Token>();

		Customer currentCustomer = repo.getCustomer(customerId);

		if (currentCustomer == null) {
			// Verify that customer has a bank account via Account Manager
			String response = rpcInterface.call(customerId);
			if (response.equals("true")) {
				currentCustomer = new Customer(customerId);
				repo.addCustomer(currentCustomer);
			} else {
				throw new Exception("No bank account associated with CPR number");
			}
		} else {
			List<Token> oldTokens = currentCustomer.getTokens();

			int unusedTokens = 0;

			for (int i = 0; i < oldTokens.size(); i++) {
				if (oldTokens.get(i) != null && !oldTokens.get(i).isUsed()) {
					unusedTokens++;
					newTokens.add(oldTokens.get(i));
				}

				if (unusedTokens > 1) {
					throw new Exception("Too many unused tokens");
				}
			}
		} 
		// Customer may request 1-5 tokens
		for (int i = 0; i < amount; i++) {
			String uniqueString = makeUniqueString();

			Barcode barcode = BarcodeFactory.createCode128B(uniqueString);
			barcode.setBarHeight(BAR_HEIGHT);
			barcode.setBarWidth(BAR_WIDTH);

			newTokens.add(new Token(uniqueString));

			// Save in DB
			//repo.addToken(newTokens.get(i));

			//File imgFile = new File(uniqueString+".png");
			// TODO: Implement a file deletion function
			// to be called when tests are finished.

			// Write bar code to PNG file
			//BarcodeImageHandler.savePNG(barcode,  imgFile);
		}

		repo.updateCustomer(currentCustomer, newTokens);

		return newTokens;
	} 

	/**
	 * This method is called when a merchant scans a token.
	 * 
	 * @param tokenId is the unique string of the scanned token
	 * @throws Exception on already used or fake token 
	 */

	public void useToken(String tokenId) throws Exception {

		List<Token> tokens = new ArrayList<Token>();

		List<Customer> customers = repo.getAllCustomers();
		for (Customer c : customers) {
			tokens = c.getTokens();
			for (Token t : tokens) {
				if (t.getString().equals(tokenId)) {
					if (t.isUsed()) {
						throw new Exception("Token has already been used");
					}

					t.use();
					repo.updateCustomer(c, tokens);

					// TODO: direct message queue
					return;
				}
			}
		}
		throw new Exception("Token does not exist");
	}
	
	/**
	 * This method creates a UUID to represent a token string.
	 * 
	 * @return a random UUID
	 */

	public String makeUniqueString(){
		return UUID.randomUUID().toString().substring(0, 10);
	}
	
	/**
	 * This method allows other classes to access the repository of the token manager.
	 * 
	 * @return a Repository
	 */

	public Repository getRepo() {
		return repo;
	}
	
	/**
	 * This method retrieves the id of the customer who owns a specific token.
	 * 
	 * @param tokenId is the id of the token
	 * @return the id of the customer who owns the token
	 */

	public String getCustomerId(String tokenId) {
		List<Token> tokens = new ArrayList<Token>();
		List<Customer> customers = repo.getAllCustomers();

		for (Customer c : customers) {
			tokens = c.getTokens();
			for (Token t : tokens) {
				if (t.getString().equals(tokenId)) {
					return c.getID();
				}
			}
		}
		return null;

	}
	
	/**
	 * This method counts who many unused tokens a customer has.
	 * 
	 * @param customerId is the id of the customer
	 * @return the number of unused tokens
	 */

	public int numberOfUnusedTokens(String customerId) {
		List<Token> tokens = repo.getCustomerTokens(customerId);
		int unused = 0;

		for (Token t : tokens) {
			if (!t.isUsed()) {
				unused++;
			}
		}
		return unused;
	}

	/**
	 * This method clears the repository.
	 */
	public void clearDatabase() {
		repo.clearRepository();
		System.out.println("Repo customer list length:" + repo.getAllCustomers().size());
	}

}
