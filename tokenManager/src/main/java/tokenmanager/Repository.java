package tokenmanager;
import java.util.List;

/**
 * Repository interface
 * Created for dependency inversion
 * 
 * @author Ole Eilgaard s154086
 *
 */

public interface Repository {

	public Customer getCustomer(String id);

	public void clearRepository(); 

	public void addCustomer(Customer customer);

	public void removeCustomer(Customer customer);

	public void updateCustomer(Customer customer, List<Token> newTokens);

	public List<Token> getCustomerTokens(String customerId);
	
	public List<Customer> getAllCustomers();
}
