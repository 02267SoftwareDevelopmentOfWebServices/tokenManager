package tokenmanager;


import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

/**
 * Resource for individual tokens
 * 
 * @author Ole Eilgaard s154086
 *
 */

@Path("/tokens/{tokenid}")
public class TokenIDResource {
	
	/**
	 * Called via REST when a merchant scans a token.
	 * Updates the token, if the token is valid.
	 * 
	 * @param tokenId is the id of the token which is scanned
	 * @return a response indicating that the token is used
	 */
	// POST, because useToken is not idempotent
	@POST
	@Produces("text/plain")
	public Response useToken(@PathParam("tokenid") String tokenId) {
		try {
			TokensResource.tm.useToken(tokenId);
			return Response.ok("Token used").build();
			
		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
	
	/**
	 * Called via REST to retrieve the id of the customer whose token was scanned.
	 * 
	 * @param tokenId is the id of the token which was scanned
	 * @return a response containing the id of the customer who owns the scanned token 
	 */
	
	@GET
	@Produces("text/plain")
	public Response getCustomerId(@PathParam("tokenid") String tokenId) {
		try {
			String customerId = TokensResource.tm.getCustomerId(tokenId);
			return Response.ok(customerId).build();
			
		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
}

