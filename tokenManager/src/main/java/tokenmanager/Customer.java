package tokenmanager;

import java.util.List;

/**
 * Customer object class
 * 
 * @author Ole Eilgaard s154086
 *
 */

public class Customer {
	private String cpr; // TODO: change type?
	private List<Token> myTokens;
	
	public Customer(String cpr) {
		this.cpr = cpr;
	}
	
	/**
	 * This method retrieves a customer's list of tokens.
	 * 
	 * @return a customer's list of tokens
	 */
	
	public List<Token> getTokens() {
		return myTokens;
	}
	
	/**
	 * This method returns the customer's CPR number.
	 * 
	 * @return the CPR number of a customer
	 */
	
	public String getID() {
		return cpr;
	}
	
	/**
	 * This method updates the list of tokens belonging to a customer.
	 * 
	 * @param tokens is the list of updated tokens
	 */
	
	public void updateTokens(List<Token> tokens) {
		myTokens = tokens;
	}
}
