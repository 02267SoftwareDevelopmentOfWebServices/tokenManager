package tokenmanager;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures application path for REST
 * 
 * @author Ole Eilgaard s154086
 *
 */

@ApplicationPath("/")
public class RestApplication extends Application {

}
