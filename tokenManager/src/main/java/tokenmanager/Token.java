package tokenmanager;

/**
 * Token object class
 * 
 * @author Ole Eilgaard s154086
 *
 */

public class Token {
	private String uniqueString;
	private boolean used;
	
	public Token(String uniqueString) {
		this.uniqueString = uniqueString;
	}
	
	/**
	 * This method retrieves the string of a token.
	 * 
	 * @return the unique string associated with the token
	 */
	
	public String getString() {
		return this.uniqueString;
	}
	
	/**
	 * This method returns whether a token is used or not.
	 * 
	 * @return a boolean indicating whether the token has been used or not
	 */
	
	public boolean isUsed() {
		return used;
	}
	
	/**
	 * This method sets the boolean field of the token.
	 * The method is called when a token is scanned.
	 */
	
	public void use() {
		used = true;
	}
}
