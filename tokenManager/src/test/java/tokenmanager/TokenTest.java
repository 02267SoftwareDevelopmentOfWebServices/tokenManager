package tokenmanager;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tokenmanager.Customer;
import tokenmanager.Token;
import tokenmanager.TokenManager;

/**
 * Internal cucumber test
 * Testing business logic of token manager
 * 
 * @author Ole Eilgaard s154086
 *
 */

public class TokenTest {

	Customer customer;
	String customerId;
	List<Token> tokens;
	String errorMessage;
	
	RPCInterface rpcInterface;
	
	TokenManager tm;

	@Before
	public void setUp() throws Exception {
		// TODO: Mock unconnected RPCClient
		rpcInterface = mock(RPCInterface.class);
		when(rpcInterface.call(anyString())).thenReturn("true");
		
		tm = new TokenManager(rpcInterface);
		tm.getRepo().clearRepository();
		customerId = "123456-7890";
		customer = new Customer(customerId);
	}

	@Given("^that I am \"([^\"]*)\"$")
	public void thatIAm(String registerStatus) throws Throwable {
		
		 if(registerStatus.equals("registered")){
		 
			tokens = tm.requestTokens(customerId,1);
			tm.useToken(tokens.get(0).getString());
		}
		 
	}
	
	@Given("^that I have (\\d+) tokens$")
	public void thatIHaveTokens(int arg1) throws Throwable {
		tokens = tm.requestTokens(customerId,arg1);
	}

	@Given("^that I have (\\d+) used tokens$")
	public void thatIHaveUsedTokens(int arg1) throws Throwable {
		for (int i = 0; i < arg1; i++) {
			tm.useToken(tokens.get(i).getString());
		}
	}
	
	@When("^I request (\\d+) tokens$")
	public void iRequestToken(int arg1) throws Throwable {
		try {
			tokens = tm.requestTokens(customerId, arg1);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}	
	}

	@When("^I use a token$")
	public void iUseToken() throws Throwable {
	    try {
	    	tm.useToken(tokens.get(0).getString());
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}

	@When("^I use a fake token$")
	public void iUseAFakeToken() throws Throwable {
	    try {
	    	 tm.useToken("fakeToken");
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}

	@Then("^I have (\\d+) unused token$")
	public void iHaveUnusedToken(int arg1) throws Throwable {
		int unusedTokens = tm.numberOfUnusedTokens(customerId);
	    assertEquals(unusedTokens, arg1);
	}
	
	@Then("^I have (\\d+) token$")
	public void iHaveToken(int arg1) throws Throwable {
		tokens = tm.getRepo().getCustomerTokens(customerId);
	    assertEquals(tokens.size(), arg1);
	}

	@Then("^I am registered$")
	public void iAmRegistered() throws Throwable {
		assertNotNull(tm.getRepo().getCustomer(customerId));
	}

	@Then("^there is an error message \"(.*?)\"$")
	public void thereIsAnErrorMessage(String message) throws Throwable {
		assertEquals(message, errorMessage);
	}
	
}
