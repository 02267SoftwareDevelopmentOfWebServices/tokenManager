package adaptertest;

/**
 * Token object class
 * 
 * @author Anna Nguyen s154230
 *
 */

public class Token {
	private String string;
	private boolean used;
	
	public Token(String uniqueString) {
		this.string = uniqueString;
	}
	
	public Token() {
		
	}
	
	public String getString() {
		return this.string;
	}
	
	public boolean isUsed() {
		return used;
	}
	
	public void use() {
		used = true;
	}
}
