package adaptertest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * How to run:
 * a) mvn thorntail:run
 * b.1) mvn package
 * b.2) java -jar demo-thorntail.jar (in mvn)
 */

/**
 * JUnit tests for external testing
 * of REST adapters of token manager
 * 
 * @author Anna Nguyen s154230
 *
 */

public class TokenManagerAdapterTest {
	Client client = ClientBuilder.newClient();
	WebTarget r;
	String cpr = "123456-7895";
	String cpr2 = "012345-6786";
	String cpr3 = "012345-6386";
	List<Token> tokens;

	@Test
	public void testRequestToken() {
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");
		// Simulates the customer inputting personal information
		JSONObject info = new JSONObject();
		info.put("firstName", "Paula");
		info.put("lastName", "Anderson");
		info.put("cpr", cpr);
		info.put("type", "customer");

		String message = info.toString();

		r.request().post(Entity.entity(message,"text/plain"),String.class);

		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens?amount=1");
		Response response = r.request().post(Entity.entity(cpr,"text/plain"),Response.class);
		tokens = response.readEntity(new GenericType<List<Token>>() {});

		assertNotNull(tokens);
		System.out.println("Tokens requested");
	}

	@Test
	public void testUseToken() {
		// Simulates the customer inputting personal information
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");
		JSONObject info = new JSONObject();
		info.put("firstName", "Anders");
		info.put("lastName", "Poulson");
		info.put("cpr", cpr2);
		info.put("type", "customer");

		String message = info.toString();
		r.request().post(Entity.entity(message,"text/plain"),String.class);

		// Request one token for cpr2
		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens?amount=1");

		Response response = r.request().post(Entity.entity(cpr2,"text/plain"),Response.class);

		tokens = response.readEntity(new GenericType<List<Token>>() {});
		
		// Retrieve one token from cpr2
		String tokenId = tokens.get(0).toString();

		// Simulates the merchant inputting personal information
		r = client.target("http://02267-istanbul.compute.dtu.dk:8081/accounts");
		info = new JSONObject();
		info.put("firstName", "Poul");
		info.put("lastName", "Polar");
		info.put("cpr", cpr3);
		info.put("type", "merchant");

		message = info.toString();
		r.request().post(Entity.entity(message,"text/plain"),String.class);
		
		// Use provided token of cpr2
		// implying that a merchant has used their token
		r = client.target("http://02267-istanbul.compute.dtu.dk:8082/tokens/"+tokenId);

		String result = r.request().post(null,String.class);

		assertEquals(result,"Token used");
		System.out.println(result);
	}
}
